
<table style="border: none">
    <tr>
        <td><a href="https://ibb.co/QNLpRPc"><img width="70" src="https://i.ibb.co/Fw2mM5W/photo-2022-07-25-18-58-09.jpg" alt="photo-2022-07-25-18-58-09" border="0" /></a></td>
        <td><b>Титов Кирилл Евгеньевич</b><br>26 лет, Москва<br>kir.titoff@gmail.com; +79772736495</td>
    </tr>
</table>

___

### ПРОЕКТЫ
<ol reversed>
    <li><div style="text-align: justify; width: 70%"><b>Обучение Java</b><br>
        Git, Maven, Java SE, MyBatis, Hibernate, PostgreSQL, Liquibase, JDBC, JPA, JSP, JTA, JPQL, JMS, Hazelcast, Spring, SpringBoot, Docker, SOAP, REST, JAX-RS, JAX-WS, JSON, XML</div></li>
    <li><div style="text-align: justify; width: 70%"><b>Проект ПАО Сбербанк Premium Solutions - OpenShift</b><br>
        2021 - 2022. Подряд от Техносерв Консалтинг<br>интеграция CRM со смежными системами (Kubernetes, Openshift)</div></li>
    <li><div style="text-align: justify; width: 70%"><b>Проект ПАО Сбербанк Premium Solutions - CRM приложение</b><br>
        2018 - 2022. Подряд от Техносерв Консалтинг<br>разработка приложения на базе фреймворка Siebel CRM: backend, frontend (OpenUI, JS, HTML, CSS), интеграция (EAI), работа с БД Oracle (выборки данных, процедуры)</div></li>
    <li><div style="text-align: justify; width: 70%"><b>Проект РСХБ - ККР Микро</b><br>
        2018. Подряд от Техносерв Консалтинг
        Разработчик CRM Siebel</div></li>
</ol>

---

### ОБРАЗОВАНИЕ
<table>
    <tr>
        <td><b>Период обучения</b></td>
        <td><b>Вуз, факальтет, специальность</b></td>
    </tr>
    <tr>
        <td>2013 - 2017 :white_check_mark:</td>
        <td>НИЯУ МИФИ, ИИКС, направление Информационная безопасность, каф. 42, бакалавриат</td>
    </tr>
    <tr>
        <td>2017 - 2019 :white_check_mark:</td>
        <td>НИЯУ МИФИ, ИИКС, направление Информатика и вычислительная техника, каф. 12, магистратура</td>
    </tr>
    <tr>
        <td>2019 - настоящее время</td>
        <td>НИЯУ МИФИ, ИИКС, направление Информатика и вычислительная техника, каф. 12, аспирантура</td>
    </tr>
</table>

### КУРСЫ
<table>
    <tr>
        <td><b>Период обучения</b></td>
        <td><b>Наименование</b></td>
    </tr>
    <tr>
        <td>2020</td>
        <td>Онлайн курс Javarush</td>
    </tr>
    <tr>
        <td>2021 - 2022</td>
        <td>ИТ-Школа Дениса Волненко JAVA STANDARD EDITION</td>
    </tr>
    <tr>
        <td>2022</td>
        <td>ИТ-Школа Дениса Волненко JAVA EE & SPRING FRAMEWORK</td>
    </tr>
</table>

---

### ИНСТРУМЕНТЫ И ТЕХНОЛОГИИ
<div style="text-align: justify; width: 70%">HTML, CSS, JS, Oracle DB, PLSQL Developer, Putty, WinSCP, Postman, Insomnia, SoapUI, SOAP, REST, Git, Maven, Java SE, MyBatis, Hibernate, PostgreSQL, Liquibase, JDBC, JPA, JSP, JTA, JPQL, JMS, Hazelcast, Spring, SpringBoot, Docker, SOAP, REST, JAX-RS, JAX-WS, JSON, XML, Docker, Kubernetes, Openshift, Jenkins, Jira, Confluence, Siebel CRM, OpenUI</div>